import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  nombre:string;
  constructor( private dataService:DataService) { }
  
  ngOnInit(): void {
  }

  vernombre(){
    this.nombre = 'Alex Medina';
    this.dataService.nombreUsuario = this.nombre;
  }
  
  cambionombreHijo(cambiaTituloNavBar : string){
    this.nombre = cambiaTituloNavBar;
    this.dataService.nombreUsuario = cambiaTituloNavBar;
  }
  
 
}
