import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styles: [
  ]
})
export class HijoComponent implements OnInit {

  @Input() vernombre:string;
  @Output() cambiarnombreHijo = new EventEmitter<string>();
  
  constructor( ) { }
  
  ngOnInit(): void {
  }
  nombreHijo(){
    this.vernombre = 'Bernardo Peña';
    this.cambiarnombreHijo.emit(this.vernombre);
    
  }
 
}
